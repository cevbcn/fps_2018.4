﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeDebug : MonoBehaviour
{
    void OnDrawGizmos()
    {
        Gizmos.DrawIcon(transform.position, "nodeDebug.png", true);
    }
}
